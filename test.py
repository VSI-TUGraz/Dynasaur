import os
import json

from dynasaur.plugins.data_visualization_controller import DataVisualizationController
from dynasaur.plugins.criteria_controller import CriteriaController


input_dir = os.path.join(os.getcwd(), "ball_board_simple_dyna")

path_to_def = os.path.join(input_dir, "calc_procedure.def")
path_to_def_id = os.path.join(input_dir, "object.def")
path_to_data = os.path.join(input_dir, "binout*")

data_vis_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                  object_def_file=path_to_def_id,
                                                  data_source=path_to_data)

commands = [
    {'visualization': 'BOARD_node_1_vel', 'x_label': 'time[ms]', 'y_label': 'x_vel'},
    {'visualization': 'BOARD_node_2_vel', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
    {'visualization': 'BOARD_node_1_vel_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
    {'visualization': 'BOARD_node_2_disp_z', 'x_label': 'time[ms]', 'y_label': 'y_vel'},
    {'visualization': 'BOARD_secforc_1_dips', 'x_label': 'time[ms]', 'y_label': 'forc'},
    {'visualization': 'BOARD_secforc_2_dips', 'x_label': 'time[ms]', 'y_label': 'forc'},
    {'visualization': 'BOARD_add_x_y', 'x_label': 'time[ms]', 'y_label': 'forc'},
]

for command in commands:
    data_vis_controller.calculate(command)

data_vis_controller.write_CSV(input_dir, filename="diagram.csv")

crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def, object_def_file=path_to_def_id,
                                     data_source=path_to_data)

commands = [{'criteria': 'BOARD_solid_obj_1_stress'},
            {'criteria': 'BOARD_solid_obj_2_stress'},
            {'criteria': 'MODEL_Internal_Energy_Max'}]

for command in commands:
    crit_controller.calculate(command)

crit_controller.write_CSV(input_dir, filename="criteria.csv")

reference_file = os.path.join(input_dir, "diagram-reference.json")
with open(reference_file) as f:
    print("[START] Comparing diagram outputs")
    if not all(sorted(json.load(f).items())) == all(sorted(data_vis_controller.get_data().items())):
        raise ValueError("Diagram Error")
    print("[END] Comparing diagram outputs")


reference_file = os.path.join(input_dir, "criteria-reference.json")
with open(reference_file) as f:
    print("[START] Comparing criteria outputs")
    if not all(sorted(json.load(f).items())) == all(sorted(crit_controller.get_data().items())):
        raise ValueError("Criteria Error")
    print("[END] Comparing criteria outputs")

