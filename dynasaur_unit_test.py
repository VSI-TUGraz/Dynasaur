import os
import io
import unittest
import sys
import glob
import argparse

from unittest import TestCase

from dynasaur.plugins.data_visualization_controller import DataVisualizationController
from dynasaur.plugins.criteria_controller import CriteriaController
from dynasaur.data.dynasaur_definitions import DynasaurDefinitions
from dynasaur.utils.logger import ConsoleLogger
from dynasaur.data.dynasaur_definitions import Units

import matplotlib.pyplot as plt
import numpy as np

class TestImpl(TestCase):
    """
    Init class contructor
    """
    def __init__(self, testname, path):
        super(TestImpl, self).__init__(testname)
        self._path = path

    def test_board_dyna(self):
        ball_board_dyna_dir = os.path.join(self._path, "LS-DYNA_dummy_ball_board_simple_data_controller")

        path_to_def_func_def = os.path.join(ball_board_dyna_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id_def = os.path.join(ball_board_dyna_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(ball_board_dyna_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

    def test_virtual_3_5_criteria(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        viva_M35_dir = os.path.join(self._path, "LS-DYNA_simulated-data_VIVA_M35_VPS-data")
        path_to_def_id = os.path.join(viva_M35_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(viva_M35_dir, "simulation_output", "binout*")
        path_to_func = os.path.join(viva_M35_dir, "auxiliaries", "funcs.def")

        assert(os.path.exists(path_to_func))

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_func,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data,
                                                 code_type="LS-DYNA")

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)


    def VPS_dummy_ball_board_simple_data_controller(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        #There is no calculation procedures file

        ball_board_vps_dir = os.path.join(self._path, "VPS_dummy_ball_board_simple_data_controller")
        path_to_def_id = os.path.join(ball_board_vps_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(ball_board_vps_dir, "simulation_output", "00_Main_Ball_Brett_01_RESULT.erfh5")
        path_to_func = os.path.join(self._path, "LS-DYNA_dummy_ball_board_simple_data_controller", "auxiliaries", "calc_proc.def")
        assert(os.path.exists(path_to_func))

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data,
                                                             code_type="VPS")

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        return

    def compare_dyna_vps_board_example(self):
        """
        testing board vps example
        test of how to implement the vps data in dynasaur
        :return:
        """
        #TODO: Check the output with Martin
        ball_board_vps_dir = os.path.join(self._path, "VPS_dummy_ball_board_simple_data_controller")
        path_to_def_id_vps = os.path.join(ball_board_vps_dir, "auxiliaries", "ids.def")
        path_to_data_vps = os.path.join(ball_board_vps_dir, "simulation_output", "00_Main_Ball_Brett_01_RESULT.erfh5")

        ball_board_dyna_dir = os.path.join(self._path, "LS-DYNA_dummy_ball_board_simple_data_controller")
        path_to_def_id_dyna = os.path.join(ball_board_dyna_dir, "auxiliaries", "ids.def")
        path_to_data_dyna = os.path.join(ball_board_dyna_dir, "simulation_output", "Convert_to_Dyna_1", "binout*")

        path_to_func = os.path.join(ball_board_dyna_dir, "auxiliaries", "calc_proc.def")
        assert(os.path.exists(path_to_func))

        dpc_dyna = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                               object_def_file=path_to_def_id_dyna,
                                               data_source=path_to_data_dyna,
                                               code_type="LS-DYNA")

        dpc_vps = DataVisualizationController(calculation_procedure_def_file=path_to_func,
                                              object_def_file=path_to_def_id_vps,
                                              data_source=path_to_data_vps,
                                              code_type="VPS")

        for command in dpc_dyna.get_defined_calculation_procedures():
            dpc_vps.calculate(command)
            dpc_dyna.calculate(command)


        for body_region in dpc_vps.get_data():
            for vis in dpc_vps.get_data(body_region):
                plt.plot(dpc_vps.get_data(body_region, vis)["X"],
                         dpc_vps.get_data(body_region, vis)["Y"],
                         label="LS-DYNA")

                plt.plot(dpc_dyna.get_data(body_region, vis)["X"],
                         dpc_dyna.get_data(body_region, vis)["Y"],
                         label="LS-DYNA")

                plt.xlabel("time [ms]")
                plt.ylabel("z displacement [mm]")
                plt.legend()

                plt.show()

        # plt.plot(dpc_vps.get_data("BOARD", "node_2_disp_z")["X"],
        #          dpc_vps.get_data("BOARD", "node_2_disp_z")["Y"],
        #          label="VPS",
        #          color="red")
        #
        # plt.plot(dpc_dyna.get_data("BOARD", "node_2_disp_z")["X"],
        #          dpc_dyna.get_data("BOARD", "node_2_disp_z")["Y"],
        #          label="LS-DYNA")


    def test_beam_mixed_elements(self):
        """
        testing with VIVA cube model
        test of how to handle mixed beam modeling (Zero integration points and multiple intregration points)
        :return:
        """
        beam_dir = os.path.join(self._path, "beam")

        path_to_def = os.path.join(beam_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(beam_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(beam_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(beam_dir, "results"))

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                             object_def_file=path_to_def_id,
                                             data_source=path_to_data)

        for command in crit_controller.get_defined_calculation_procedures():
            command["t_start"] = 0.00
            command["t_end"] = 20.0
            crit_controller.calculate(command)

        crit_controller.write_CSV(os.path.join(beam_dir, "results"))

    def test_basic_ls_functions(self):
        """
        testing with VIVA femur model
        testing injury criteria: writing out single measurement channels and filtering + basic ls_functions like "res",
        "abs", "HIC".
        Compared with Diadem
        :return:
        """
        input_dir = os.path.join(self._path, "binout_VIVA_femur")
        path_to_def = os.path.join(input_dir, "auxiliaries", "Dynasaur_Def_File_ProtectMe_H350_Filter.def")
        path_to_def_id = os.path.join(input_dir, "auxiliaries", "Dynasaur_Def_File_ProtectMe_H350_Filter_ID.def")

        path_to_data = os.path.join(input_dir, "simulation_output", "binout*")

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                             object_def_file=path_to_def_id,
                                             data_source=path_to_data)

        for command in crit_controller.get_defined_calculation_procedures():
            crit_controller.calculate(command)
        crit_controller.write_CSV(os.path.join(input_dir, "results"))

        data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                      object_def_file=path_to_def_id,
                                                      data_source=path_to_data)

        for command in data_controller.get_defined_calculation_procedures():
            data_controller.calculate(command)

        data_controller.write_CSV(os.path.join(input_dir, "results"))

    def test_injuries_on_hbm(self):
        """
        testing with VIVA model
        testing injury criteria: HIC, BRIC
        :return:
        """
        # **********************************************************************************************************************

        input_dirs = [os.path.join(self._path, "binout_VIVA_Example")]

        for input_dir in input_dirs:
            path_to_def = os.path.join(input_dir, "auxiliaries", "VIRTUAL_VIVA_Dynasaur_Def.def")
            path_to_def_id = os.path.join(input_dir, "auxiliaries", "VIRTUAL_VIVA_Dynasaur_Def_ID.def")
            path_to_data = os.path.join(input_dir, "simulation_output", "binout*")

            data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                          object_def_file=path_to_def_id,
                                                          data_source=path_to_data)

            for command in data_controller.get_defined_calculation_procedures():
                data_controller.calculate(command)
            data_controller.write_CSV(os.path.join(input_dir, "results"))

            crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data)

            for command in crit_controller.get_defined_calculation_procedures():
                crit_controller.calculate(command)
            crit_controller.write_CSV(os.path.join(input_dir, "results"))



    def test_beam_failed_elements(self):
        """
        testing with battery model
        consists of elout beam elements (single integration point) ... failing over time and get deleted
        """
        input_dir = os.path.join(self._path, "battery")
        path_to_def = os.path.join(input_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(input_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(input_dir, "simulation_output", "binout*")

        data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                      object_def_file=path_to_def_id,
                                                      data_source=path_to_data)

        for command in data_controller.get_defined_calculation_procedures():
            command["t_start"] = 20.00
            data_controller.calculate(command)

    def test_beam_del_elements(self):
        """

        :return:
        """
        input_dir = os.path.join(self._path, "binout_beams_deleted")
        path_to_def = os.path.join(input_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(input_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(input_dir, "simulation_output", "binout*")

        data_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                      object_def_file=path_to_def_id,
                                                      data_source=path_to_data)

        for command in data_controller.get_defined_calculation_procedures():
            data_controller.calculate(command)

    def test_create_default_object_definitions(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_femur")

        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")
        from dynasaur.data.dynasaur_definitions import Helpers
        d = Helpers.create_default_object_definitions(data_source=path_to_data, code_type="LS-DYNA")

        return

    def test_femur_output_files(self):
        """
       testing with femur model
       testing min, max standard functions and compare the output file with reference files
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data)
        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)

        criteria_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

        ref_data_vis = os.path.join(path_sim_sim_dir, "reference", "PLUGIN_DATA_VISUALISATION_ref.csv")

        list_of_files_data_vis = glob.glob(path_sim_sim_dir + "\\results\\PLUGIN_DATA_VISUALISATION_*.csv")
        latest_file_data_vis = max(list_of_files_data_vis, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_data_vis, latest_file_data_vis, self)

        ref_kinematic_crit = os.path.join(path_sim_sim_dir, "reference", "PLUGIN_CRITERIA_ref.csv")

        list_of_files_kinematic_crit = glob.glob(path_sim_sim_dir + "\\results\\PLUGIN_CRITERIA_*.csv")
        latest_file_kinematic_crit = max(list_of_files_kinematic_crit, key=os.path.getctime)

        self.maxDiff = None
        HelpClass.check_diff_files(ref_kinematic_crit, latest_file_kinematic_crit, self)


    def test_method_get_object_data(self):
        path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_ID.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data)

        lower_surface_x = criteria_controller.get_object_data(type_="NODE",
                                                              id_="Lower_Surface",
                                                              strain_stress=None,
                                                              index=0,
                                                              channel="x_coordinate",
                                                              data_offset=(0, -1))

        lower_surface_y = criteria_controller.get_object_data(type_="NODE",
                                                              id_="Lower_Surface",
                                                              strain_stress=None,
                                                              index=0,
                                                              channel="y_coordinate",
                                                              data_offset=(0, -1))

        print(lower_surface_x.shape)
        print(lower_surface_y.shape)

        plt.plot(lower_surface_x.flatten(), lower_surface_y.flatten())
        plt.show()

    def test_example_solid_cube(self):
        """
       testin with simple cube model
       testing max functions from nodout, section, glstat, matsum and Part IDs  (ELOUT)  # Stress, Strain Assessment
       """
        path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_ID.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)
        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

        plt.plot(data_plugin_controller.get_data()["CUBE"]["stress_strain_time_history_max"]["X"],
                 data_plugin_controller.get_data()["CUBE"]["stress_strain_time_history_max"]["Y"])
        plt.show()

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data)

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)

        criteria_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

    def test_definition_name_incorrect(self):
        """
       test error handling
       testing definition file validator
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur_exit.def")
        capturedOutput = io.StringIO()  # Create StringIO object
        sys.stdout = capturedOutput
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(path_to_def)
        except AssertionError:
            pass

    def test_invalid_json_file(self):
        """
       test error handling
       testing if tool throw the right error message, invalid json file
       """

        path_sim_sim_dir = os.path.join(self._path, "binout_femur")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur_invalid_json.def")
        dynasaur_def = DynasaurDefinitions(ConsoleLogger())
        try:
            dynasaur_def.read_def(path_to_def)
        except SystemExit:
            pass

    def test_csdm_function(self):
        """
       test csdm standard function
       testing if values are same as in reference file
       """
        path_sim_sim_dir = os.path.join(self._path, "binout_CSDM")
        path_to_def_csdm = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur.def")
        path_to_def_csdm_id = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur_ids.def")
        path_to_def_volume = os.path.join(path_sim_sim_dir, "auxiliaries", "Volume.def")
        path_to_data_csdm = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def_csdm,
                                                 object_def_file=path_to_def_csdm_id,
                                                 data_source=path_to_data_csdm,
                                                 volume_def_file=path_to_def_volume)

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)

        criteria_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))
        data = criteria_controller.get_data()

        import glob
        import pickle
        list_of_input_params = glob.glob(os.path.join(path_sim_sim_dir, "reference", "output_*.pkl"))
        dict_params = {}
        for param in list_of_input_params:
            with open(param, "rb") as pkl_file:
                dict_params.update({param: pickle.load(pkl_file)})

        # self.assertEqual(dict_params[list_of_input_params[0]]['csmd'],
        #                  criteria_controller.get_data(criteria_name='csdm_ais1+_lim0.01')['Value'])
        # self.assertEqual(dict_params[list_of_input_params[1]]['csmd'],
        #                  criteria_controller.get_data(criteria_name='csdm_ais3+_lim0.01')['Value'])
        # self.assertEqual(dict_params[list_of_input_params[2]]['csmd'],
        #                  criteria_controller.get_data(criteria_name='Rcsdm_ais3+_lim0.002')['Value'])
        # self.assertEqual(dict_params[list_of_input_params[3]]['csmd'],
        #                  criteria_controller.get_data(criteria_name='Rcsdm_ais5+_lim0.002')['Value'])

    def test_bndout(self):
        input_dir = os.path.join(self._path, "binout_bndout")
        path_to_def = os.path.join(input_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(input_dir, "auxiliaries", "ids.def")

        data_source = os.path.join(input_dir, "simulation_output", "binout*")

        data_vis_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                          object_def_file=path_to_def_id,
                                                          data_source=data_source)

        for calc in data_vis_controller.get_defined_calculation_procedures():
            data_vis_controller.calculate(calc)

        print(data_vis_controller.get_data())


    def test_rbdout(self):
        input_dir = os.path.join(self._path, "binout_rbdout")
        path_to_def = os.path.join(input_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(input_dir, "auxiliaries", "ids.def")

        data_source = os.path.join(input_dir, "simulation_output", "binout*")

        data_vis_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                             object_def_file=path_to_def_id,
                                             data_source=data_source)

        for calc in data_vis_controller.get_defined_calculation_procedures():
            data_vis_controller.calculate(calc)

        print(data_vis_controller.get_data())

    def test_joint_frc(self):
        input_dir = os.path.join(self._path, "binout_EuroNCAP")
        path_to_def = os.path.join(input_dir, "auxilaries", "Calculation_procedures_LS_FE.def")
        path_to_def_id = os.path.join(input_dir, "auxilaries", "Objects_LS_FE.def")

        # Simulation Results - which simulation do you want to analyse?
        path_to_data = os.path.join(input_dir, "simulation_output", "ENCAP_results_50th_occ")
        data_source = os.path.join(path_to_data, "binout*")

        crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                             object_def_file=path_to_def_id,
                                             data_source=data_source)

        for command in crit_controller.get_defined_calculation_procedures():
            crit_controller.calculate(command)

    def test_merge_def_files(self):
        path_sim_sim_dir = os.path.join(self._path, "SolidCube_Parameter_Set_1")

        ls_path_to_def_calc = [os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_calc_1.def"),
                               os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_calc_2.def")]

        ls_path_to_def_id = [os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_id_1.def"),
                             os.path.join(path_sim_sim_dir, "auxiliaries", "CUBE_SIM_JSON_id_2.def")
                             ]

        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_vis_controller = DataVisualizationController(calculation_procedure_def_file=ls_path_to_def_calc,
                                                          object_def_file=ls_path_to_def_id,
                                                          data_source=path_to_data)

        commands_data_vis_controller = data_vis_controller.get_defined_calculation_procedures()
        for command in commands_data_vis_controller:
            data_vis_controller.calculate(command)

        crit_controller = CriteriaController(calculation_procedure_def_file=ls_path_to_def_calc,
                                                          object_def_file=ls_path_to_def_id,
                                                          data_source=path_to_data)

        commands_crit_controller = crit_controller.get_defined_calculation_procedures()
        for command in commands_crit_controller:
            crit_controller.calculate(command)

    def test_abstat(self):
        path_to_def = []
        path_sim_sim_dir = os.path.join(self._path, "binout_ABSTAT")
        path_to_def.append(os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def"))
        path_to_def.append(os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc_2.def"))
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "baseline", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        commands_data_vis_controller = data_plugin_controller.get_defined_calculation_procedures()

        for command in commands_data_vis_controller:
            data_plugin_controller.calculate(command)

    def test_abstat_cpm(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_ABSTAT_CPM")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "baseline", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

    def test_single_element(self):
        path_sim_sim_dir = os.path.join(self._path, "single_element")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                    object_def_file=path_to_def_id,
                                                    data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        elem = data_plugin_controller.get_object_data(type_="OBJECT",
                                                      id_="solid_obj_1",
                                                      strain_stress="Strain",
                                                      index=None,
                                                      channel=None,
                                                      data_offset=(0, 31))

        reference_curve = os.path.join(path_sim_sim_dir, "reference", "curve.csv")
        my_data = np.genfromtxt(reference_curve, delimiter=' ')
        max_principle = np.max(elem["part_data"][1], axis=2)

        plt.plot(elem["time"], max_principle.flatten(), label='Dynasaur')
        plt.plot(my_data[:, 0], my_data[:, 1], label='LS-Prepost')
        plt.grid()
        plt.xlabel("time[ms]")
        plt.ylabel("MPS")
        plt.savefig("xy.png")
        plt.legend()
        plt.show()
        print("xy")

    def test_eroded_elements(self):
        #TODO: Error with part ids
        pass
        # path_sim_sim_dir = os.path.join(self._path, "binout_bug_180109")
        # path_to_def = os.path.join(path_sim_sim_dir, "calc.def")
        # path_to_def_id = os.path.join(path_sim_sim_dir, "ids.def")
        # path_to_data = os.path.join(path_sim_sim_dir, "binout*")
        #
        # crit_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
        #                                                      object_def_file=path_to_def_id,
        #                                                      data_source=path_to_data)
        #
        # commands = [
        #             #{'criteria': 'VEHICLE_COLLISIONSPEED'},
        #             {'criteria': 'VEHICLE_UNIVEXAMPLE2', "t_start": 0.00, "t_end": 0.20},
        #             {'criteria': 'VEHICLE_PERCENTILE', "t_start": 0.00, "t_end": 0.20}
        #             ]
        #
        # for command in commands:
        #     crit_controller.calculate(command)
        # crit_controller.write_CSV(path_sim_sim_dir)

    def test_rib_prob(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_HBM_example_04_Output_2ms_2steps-Rib")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "ribs.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ribs_ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        left_ribs = data_plugin_controller.get_object_data(type_="OBJECT",
                                                           id_="Left Ribs",
                                                           strain_stress="Strain",
                                                           index=None,
                                                           channel=None,
                                                           data_offset=(0, 20))

        from dynasaur.calc.standard_functions import StandardFunction

        percentile_value = StandardFunction.percentile(left_ribs,
                                                       selection_tension_compression="Tension",
                                                       integration_point="Min",
                                                       percentile=0.75)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

    def test_ASI_THIV(self):
        path_sim_dir = os.path.join(self._path, "bsp_asi_thiv")

        # path_to_def = os.path.join(_path, "ASI.def")
        path_to_def = os.path.join(path_sim_dir, "auxiliaries", "ASI_THIV.def")
        path_to_def_id = os.path.join(path_sim_dir, "auxiliaries", "ASI_ids.def")
        path_to_data = os.path.join(path_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        our_data_velocity = data_plugin_controller.get_data("HEAD", "thiv_f")
        ref2_data = np.genfromtxt(os.path.join(path_sim_dir, 'reference', "ASI_aus_acc_diagram.csv"), delimiter="\t")

        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)


    def test_ISO_MME(self):
        path_sim_sim_dir = os.path.join(self._path, "SUV_40kph_AM50_HC")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "dynasaur_ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_ISO_MME(path_to_dir=os.path.join(path_sim_sim_dir, "results"), test=True)


    def test_id_range(self):
        path_sim_sim_dir = os.path.join(self._path, "SUV_40kph_AM50_HC")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "test_total_sum.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "test_total_sum_id.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "simulation_output"))

    def test_element_set(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_HBM_example_04_Output_2ms_2steps-Rib")
        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "ribs.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ribs_ids_elem_set.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)

        #left_ribs = data_plugin_controller.get_object_data(type_="OBJECT",
        #                                                   id_="Left Ribs",
        #                                                   strain_stress="Strain",
        #                                                   index=None,
        #                                                   channel=None,
        #                                                   data_offset=(0, 20))

        from dynasaur.calc.standard_functions import StandardFunction

        # percentile_value = StandardFunction.percentile(left_ribs,
        #                                                selection_tension_compression="Tension",
        #                                                integration_point="Min",
        #                                                percentile=0.75)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            print(command)
            if command["visualization"].startswith("CHEST_Rib_element"): # in ["CHEST_R_Ribs_risk", "CHEST_Element_89059638_strain"]:#, "CHEST_max_strain_part_ribs"]:
                data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))


    def test_madymo_implementation_psg_48deg_UVa_v18_PMHS_Gravity(self):
        path_sim_sim_dir = os.path.join(self._path, "madymo_osccar")

        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "objects.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "2021_02_26_madymo_out_output", "48deg_UVa_v18_PMHS_GravityZ_footrest",
                                    "a_act50fc_osccar_psg_48deg_UVa_v18_PMHS_GravityZ.h5")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data,
                                                             code_type="MADYMO")

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

    def test_madymo_implementation_a_act50fc(self):
        path_sim_sim_dir = os.path.join(self._path, "madymo_bosch")

        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "objects.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output",
                                    "a_act50fc_osccar_drv.h5")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data,
                                                             code_type="MADYMO")

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))

    def test_madymo_input_reader(self):
        from dynasaur.data.madymo import MadymoData

        path_sim_dir = os.path.join(self._path, "madymo_bosch")
        path_to_data = os.path.join(path_sim_dir, "simulation_output",
                                    "a_act50fc_osccar_drv.h5")

        d = MadymoData(path_to_data)

        print(d.read())

        return

    def test_green_lagrange_implementation(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_green_lagrange")

        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "Calculation_procedures_PIPER.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "Objects_PIPER.def")

        for i in ["PIPER_use_case1", "PIPER_use_case2", "PIPER_use_case3"]:
            path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", i, "*binout*")

            # read results files
            d = np.genfromtxt(os.path.join(path_sim_sim_dir, "simulation_output", i, "1st_green_strain_at_time_100.txt")
                              , delimiter=",")
            elem_ids = d[:, 0].astype(np.int)
            data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                                 object_def_file=path_to_def_id,
                                                                 data_source=path_to_data,
                                                                 code_type="LS-DYNA")

            for command in data_plugin_controller.get_defined_calculation_procedures():
                data_plugin_controller.calculate(command)

            for nr, elem_id in enumerate(elem_ids):
                y = data_plugin_controller.get_data("HEAD", f"elem_hist_{elem_id}")["Y"]
                print(elem_id, y[1000], d[nr, 1])

            data_plugin_controller.get_data("HEAD", "elem_hist_1507798")
            data_plugin_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))


    def test_defined_object_not_in_simulation(self):
        ball_board_dyna_dir = os.path.join(self._path, "LS-DYNA_dummy_ball_board_simple_data_controller")

        path_to_def_func_def = os.path.join(ball_board_dyna_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id_def = os.path.join(ball_board_dyna_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(ball_board_dyna_dir, "simulation_output", "binout*")

        data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)

        for command in data_plugin_controller.get_defined_calculation_procedures():
            data_plugin_controller.calculate(command)

        data_plugin_controller.write_CSV(os.path.join(ball_board_dyna_dir, "results"))

    def test_head_impact_conditions(self):
        path_sim_sim_dir = os.path.join(self._path, "binout_Head_impact_conditions")

        path_to_def = os.path.join(path_sim_sim_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path_sim_sim_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_sim_sim_dir, "simulation_output", "binout*")

        commands = [
            {"criteria": "HEAD_HIVEL"},
            {"criteria": "HEAD_HIVEL_Vehicle_speed_sub"},
            {"criteria": "HEAD_HIT_Ang_vel"},
            {"criteria": "HEAD_HIT"},
            {"criteria": "HEAD_HIT_VEL_X"},
            {"criteria": "HEAD_HIT_VEL_Y"},
            {"criteria": "HEAD_HIT_VEL_Z"},
            {"criteria": "HEAD_HIT_VEL_VEH"},
            {"criteria": "HEAD_HIANG_XY_Rel"}
        ]
        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def,
                                                 object_def_file=path_to_def_id,
                                                 data_source=path_to_data)


        for command in commands:
            criteria_controller.calculate(command)

        criteria_controller.write_CSV(os.path.join(path_sim_sim_dir, "results"))
        print(criteria_controller.get_data())

    def test_data_hierarchies(self):
        path_to_dir = os.path.join(self._path, "binout_joint_joint_stiffness")
        path_to_def_func_def = os.path.join(path_to_dir, "auxiliaries", "calc_proc.def")
        path_to_def_id_def = os.path.join(path_to_dir, "auxiliaries", "ids.def")
        path_to_data = os.path.join(path_to_dir, "simulation_output", "binout*")

        # data_plugin_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
        #                                                      object_def_file=path_to_def_id_def,
        #                                                      data_source=path_to_data)
        criteria_controller = CriteriaController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)

        data_vis_controller = DataVisualizationController(calculation_procedure_def_file=path_to_def_func_def,
                                                             object_def_file=path_to_def_id_def,
                                                             data_source=path_to_data)

        for command in criteria_controller.get_defined_calculation_procedures():
            criteria_controller.calculate(command)

        for command in data_vis_controller.get_defined_calculation_procedures():
            data_vis_controller.calculate(command)

        criteria_controller.write_CSV(os.path.join(path_to_dir, "results"))
        print(criteria_controller.get_data())


class HelpClass:
    @staticmethod
    def check_diff_files(reference_file, current_file, self):
        with open(reference_file) as reference:
            with open(current_file) as output:
                self.maxDiff = None
                self.assertEqual(reference.read(), output.read())

    def initialise_controller(self, path, ids="ids.def", calc_proc="calc_proc", data_vis=False, criteria=False):
        path_to_def = os.path.join(path, "auxiliaries", "calc_proc.def")
        path_to_def_id = os.path.join(path, "auxiliaries", "objects.def")
        path_to_data = os.path.join(path, "simulation_output", "binout*")

        if data_vis:
            return_ = DataVisualizationController(calculation_procedure_def_file=path_to_def,
                                                             object_def_file=path_to_def_id,
                                                             data_source=path_to_data)


class ReadableDir(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir=values
        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a valid path".format(prospective_dir))
        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError("readable_dir:{0} is not a readable dir".format(prospective_dir))


def test_full(path):
    suite = unittest.TestSuite()
    test_loader = unittest.TestLoader()
    test_names = test_loader.getTestCaseNames(TestImpl)

    for test_name in test_names:
        suite.addTest(TestImpl(test_name, path))

    return suite




def test_bugs(path):
    suite = unittest.TestSuite()
    #suite.addTest(TestImpl('test_beam_mixed_elements', path))
    #suite.addTest(TestImpl('test_board_dyna', path))
    #suite.addTest(TestImpl('test_defined_object_not_in_simulation', path))
    suite.addTest(TestImpl('test_basic_ls_functions', path))
    suite.addTest(TestImpl('test_example_solid_cube', path))
    suite.addTest(TestImpl('test_injuries_on_hbm', path))
    suite.addTest(TestImpl('test_merge_def_files', path))
    suite.addTest(TestImpl('test_rib_prob', path))
    suite.addTest(TestImpl('test_virtual_3_5_criteria', path))
    suite.addTest(TestImpl('test_beam_mixed_elements', path))
    suite.addTest(TestImpl('test_joint_frc', path))
    suite.addTest(TestImpl('test_single_element', path))

    #suite.addTest(TestImpl('test_madymo_implementation_psg_48deg_UVa_v18_PMHS_Gravity', path))
    #suite.addTest(TestImpl('test_single_element', path))
    #suite.addTest(TestImpl('test_ASI_THIV', path))
    #suite.addTest(TestImpl('test_joint_frc', path))
    #suite.addTest(TestImpl('test_virtual_3_5_criteria', path))
    return suite


def test_beam_del_elems(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_beam_del_elements', path))
    return suite


def test_beam_del_elems_zero_ips(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_beam_failed_elements', path))
    return suite


def test_viva_(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_injuries_on_hbm', path))
    return suite


def test_stress_strain_based_hbm_criteria(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_strain_stress_criteria_hbm', path))
    return suite


def test_viva_femur(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_basic_ls_functions', path))
    return suite


def test_all_data_types(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_example_solid_cube', path))
    return suite


def test_madymo(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_madymo_implementation_a_act50fc', path))
    suite.addTest(TestImpl('test_madymo_implementation_psg_48deg_UVa_v18_PMHS_Gravity', path))
    return suite


def test_sufehm(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_sufehm', path))
    return suite


def test_virtual_vps(path):
    suite = unittest.TestSuite()
    # suite.addTest(TestImpl('test_virtual_3_5_criteria', path))
    suite.addTest(TestImpl('test_board_simple_vps', path))
    return suite


def test_compare_dyna_vps(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('compare_dyna_vps_board_example', path))
    #suite.addTest(TestImpl('test_create_default_object_definitions', path))
    return suite


def test_simple_dyna(path):
    suite = unittest.TestSuite()

    # suite.addTest(TestImpl('test_board_dyna', path))
    suite.addTest(TestImpl('test_defined_object_not_in_simulation', path))
    # suite.addTest(TestImpl('test_virtual_3_5_criteria', path))
    return suite


def test_ribs_prob_broken(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_element_set', path))
    #suite.addTest(TestImpl('test_rib_prob', path))
    return suite


def test_iso_mme(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_ISO_MME', path))
    return suite


def test_id_range(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_id_range', path))
    return suite


def test_ASI_THIV(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_ASI_THIV', path))
    return suite


def test_basic_ls_functions(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_basic_ls_functions', path))
    return suite

def test_head_impact_function(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_head_impact_conditions', path))

    return suite

def test_data_hierarchies(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_data_hierarchies', path))
    return suite


def test_green_lagrange_implementation(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_green_lagrange_implementation', path))
    return suite


def test_joint_type(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_data_hierarchies', path))
    return suite


def test_madymo_input_reader(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_madymo_input_reader', path))
    return suite


def test_csdm(path):
    suite = unittest.TestSuite()
    suite.addTest(TestImpl('test_csdm_function', path))
    return suite


if __name__ == "__main__":
    path = "."
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--path", action=ReadableDir, required=True, help="path to unit test_library")
    parser.add_argument("-t", "--type", action='store', default="test_full", required=False, help="name of test suite")

    args = parser.parse_args()

    suite = locals()[args.type](args.path)
    result = unittest.TextTestRunner().run(suite)
    sys.exit(not result.wasSuccessful())
